const fs = require('fs')
const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const CopyPlugin = require('copy-webpack-plugin')

let htmlFiles = fs.readdirSync(path.resolve(__dirname, '../../src/')).filter(item => item.match(/\.(html)/))

const htmlWebpackPlugins = htmlFiles.map(filename => new HtmlWebpackPlugin({
    filename,
    template: path.resolve(__dirname, '../../src/' + filename)
}))


let cssFiles = fs.readdirSync(path.resolve(__dirname, '../../src/css')).filter(item => item.match(/\.(scss|css)/))

const entry = [
    path.resolve(__dirname, '../../src/js/index'),
    ...cssFiles.map(filename => path.resolve(__dirname, '../../src/css/' + filename))
]

// console.log(htmlWebpackPlugins);process.exit();
 

const config = {
    mode: 'development',
    entry,
    output: {
        path: path.resolve(__dirname + '/../../dist'),
        filename: 'assets/[name].[chunkhash:8].js'
    },
    module: {
        rules: [
            {
                test: /\.ts?$/,
                exclude: /node_modules/,
                use: {
                    loader: 'ts-loader'
                }
            },
            {
                test: /\.scss$/,
                use: [
                    process.env.NODE_ENV === 'production' ? {
                        loader: MiniCssExtractPlugin.loader,
                        options: {
                        },
                      } : 'style-loader',
                    'css-loader?url=false',
                    'sass-loader'
                ]
            },
			{
				test: /\.(eot|otf|ttf|woff|woff2|svg|png|jpg)$/,
				use: 'file-loader',
			},
        ]
    },
    resolve: {
        extensions: ['.ts', '.js']
    },
    plugins: [
        new CopyPlugin({
          patterns: [
            {
                from: path.resolve(__dirname + '/../../public'),
                to: '.'
            }
          ]
        }),
        ...htmlWebpackPlugins,
        new MiniCssExtractPlugin({
            filename: 'assets/[name].[chunkhash:8].css',
            chunkFilename: '[id].css',
            ignoreOrder: false
        }),
    ]
}

module.exports = config