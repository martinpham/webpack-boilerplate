const path = require('path')
const config = require('../_common/config')

config.mode = 'development'

config.output.sourceMapFilename = 'assets/[name].js'
config.devtool = 'source-map'

config.devServer = {
  contentBase: path.join(__dirname, '../../public/'),
  compress: true,
  publicPath: '/',
  port: 9000
}

module.exports = config